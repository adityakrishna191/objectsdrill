const { isObject } = require("./keys")

function mapObject(obj, cb) {
    // Like map for arrays, but for objects. Transform the value of each property in turn by passing it to the callback function.
    // http://underscorejs.org/#mapObject

    if(!isObject(obj) || cb === undefined) return {};

    // const resultObj = {};

    for(let keys in obj) {
        
        obj[keys] = cb(obj[keys], keys, obj);

    }

    // return resultObj;

    return obj;

}

module.exports = mapObject;