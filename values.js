const { isObject } = require("./keys");

function values(obj) {
    // Return all of the values of the object's own properties.
    // Ignore functions
    // http://underscorejs.org/#values

    if(!isObject(obj)) return {};

    const result = [];

    for(let keys in obj) {
       if(typeof obj[keys] === "function"){
           continue;
       } else {
           result.push(obj[keys]);
       }
    }

    return result;

}

module.exports = values;