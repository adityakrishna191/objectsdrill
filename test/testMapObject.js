const mapObject = require("../mapObject");

const callBackFunction = (value, key, object) => {
    return value*2;
}

const result = mapObject({one: 1, two: 2, three: 3}, callBackFunction);
console.log(result);