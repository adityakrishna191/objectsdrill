const { isObject } = require("./keys");

function defaults(obj, defaultProps) {
    // Fill in undefined properties that match properties on the `defaultProps` parameter object.
    // Return `obj`.
    // http://underscorejs.org/#defaults

    if(!isObject(obj) || !isObject(defaultProps)) return {};

    for(let keys in defaultProps) {
        if(obj[keys] === undefined){
            obj[keys] = defaultProps[keys];
        }
    }

    return obj;

}

module.exports = defaults;