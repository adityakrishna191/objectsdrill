const { isObject } = require("./keys");

function pairs(obj) {
    // Convert an object into a list of [key, value] pairs.
    // http://underscorejs.org/#pairs

    if(!isObject(obj)) return {};

    const resultPairArrays = [];
    
    for(let keys in obj) {
        let pairArray = [];
        pairArray.push(keys, obj[keys]);
        resultPairArrays.push(pairArray);
    }

    return resultPairArrays;

}

module.exports = pairs;