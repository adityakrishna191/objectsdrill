const { isObject } = require("./keys");
const pairs = require("./pairs");

function invert(obj) {
    // Returns a copy of the object where the keys have become the values and the values the keys.
    // Assume that all of the object's values will be unique and string serializable.
    // http://underscorejs.org/#invert

    if(!isObject(obj)) return {};

    const invertedObject = {};

    const pairObjects = pairs(obj);
    for(let i=0; i<pairObjects.length; i++) {
        invertedObject[pairObjects[i][1]] = pairObjects[i][0];
    }

    return invertedObject;

}

module.exports = invert;