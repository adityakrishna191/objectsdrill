//To check if the argument is an object, reference: https://attacomsian.com/blog/javascript-check-variable-is-object
const isObject = (object) => {
    return Object.prototype.toString.call(object) === "[object Object]";
}

function keys(obj) {
    // Retrieve all the names of the object's properties.
    // Return the keys as strings in an array.
    // Based on http://underscorejs.org/#keys

    if(!isObject(obj)) return {};

    const keysArr = [];

    for (let keys in obj) {
        keysArr.push(keys);
    }

    return keysArr;

}

module.exports = {
    isObject, 
    keys
};